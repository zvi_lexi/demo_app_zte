package com.lexifone.sdk;

/**
 * Created by Zvi on 5/13/2015.
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

class Broadcast {
    private Context context;

    Broadcast(Context context){
        this.context = context;
    }

    public void sendSystemMessage(String message) {
        Bundle bundle = new Bundle();
        bundle.putString(LexifoneManager.MSG_KEY, message);
        send(bundle, LexifoneManager.EVENT_SYSTEM_MSG_FILTER);
    }

    public void sendSystemMessage(String message, String code, String description){
        Bundle bundle = new Bundle();
        bundle.putString(LexifoneManager.MSG_KEY, message);
        bundle.putString(LexifoneManager.SYSTEM_MSG_CODE, code);
        bundle.putString(LexifoneManager.SYSTEM_MSG_DESCRIPTION, description);
        send(bundle, LexifoneManager.EVENT_SYSTEM_MSG_FILTER);
    }

    public void sendConnectStatus(String status){
        Bundle bundle = new Bundle();
        bundle.putString(LexifoneManager.MSG_KEY, status);
        send(bundle, LexifoneManager.EVENT_CONNECT_FILTER);
    }

    public void sendChangeLanguagesStatus(String status){
        Bundle bundle = new Bundle();
        bundle.putString(LexifoneManager.MSG_KEY, status);
        send(bundle, LexifoneManager.EVENT_CHANGE_LANGS_FILTER);
    }

    public void sendTranslateResultMsg(String transcript, Bundle resultBundle){
        Bundle bundle = new Bundle();
        bundle.putString(LexifoneManager.NATIVE_TRANSCRIPT_KEY, transcript);
        bundle.putBundle(LexifoneManager.MSG_KEY, resultBundle);
        send(bundle, LexifoneManager.EVENT_TRANSLATE_RESULT_FILTER);
    }

    public void sendAudioData(byte[] data){
        Bundle bundle = new Bundle();
        bundle.putByteArray(LexifoneManager.MSG_KEY, data);
        send(bundle, LexifoneManager.EVENT_AUDIO_DATA_FILTER);
    }

    private void send(Bundle bundle, String intentFilter){
        if(context == null){
            return;
        }
        Intent intent = new Intent(intentFilter);
        intent.putExtras(bundle);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

//    private Handler mHandler = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            switch (msg.what) {
//                case LexifoneManager.CONNECTED:
////                    encodeBT.setVisibility(android.view.View.INVISIBLE);
////                    sendAuthentiateMessage();
//                    break;
//                case LexifoneManager.DISCONNECTED:
////				encodeBT.setVisibility(android.view.View.VISIBLE);
////                    connectStatusTV.setText("connect status:disconnected");
//                    break;
//                case LexifoneManager.AUTHENTICATE_SUCCESS:
////                    connectStatusTV.setText("validate");
//                    break;
//                case LexifoneManager.SWAP_LANGUAGE:
////                    StringBuilder builder = new StringBuilder();
////                    builder.append("native:");
////                    builder.append(descriptor.getNativeLanguage());
////                    builder.append(",foreign:");
////                    builder.append(descriptor.getForeignLanguages()[0]);
////                    String status = msg.getData().getString(
////                            LexifoneManager.TEXT_RESULT_KEY);
////                    builder.append(",status:");
////                    builder.append(status);
////                    connectStatusTV.setText(builder.toString());
//                    break;
//                case LexifoneManager.TEXT_TRANSLATION_RESULT:
////                    Bundle bundle = msg.getData();
////                    Map<String, String> map = (Map<String, String>) bundle
////                            .getSerializable(LexifoneManager.TEXT_RESULT_KEY);
////                    String resultString = "";
////                    for (String language : descriptor.getForeignLanguages()) {
////                        if (map.containsKey(language)) {
////                            resultString += map.get(language) + "\n";
////                        }
////                    }
////                    // String text = outTV.getText().toString() + resultString;
////                    // outTV.setText(text);
////                    outTV.setText(resultString);
////
//                    break;
//                case LexifoneManager.AUDIO_TO_AUDIO:
////                    Bundle audioBundle = msg.getData();
////                    byte[] data = audioBundle
////                            .getByteArray(LexifoneManager.TEXT_RESULT_KEY);
////                    playMeida(data);
//                    break;
//                default:
//                    break;
//            }
//        }
//    };
//
//    public Handler getHandler(){
//        return this.mHandler;
//    }
}

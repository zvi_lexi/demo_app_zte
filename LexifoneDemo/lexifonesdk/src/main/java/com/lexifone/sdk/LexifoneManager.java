package com.lexifone.sdk;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.purplefrog.speexjni.FrequencyBand;
import com.purplefrog.speexjni.SpeexDecoder;
import com.purplefrog.speexjni.SpeexEncoder;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import de.tavendo.lexifone.WebSocket.WebSocketConnectionObserver;
import de.tavendo.lexifone.WebSocketConnection;
import de.tavendo.lexifone.WebSocketOptions;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Zvi on 5/13/2015.
 * the class provides access to lexifone sdk functionality.
 * the calling application must use the getInstance method
 * to get a reference to its singletone object and then use
 * its public methods to process application requests.
 */
public class LexifoneManager {

    public static final String EVENT_SYSTEM_MSG_FILTER = "system.message";
    public static final String EVENT_CONNECT_FILTER = "connect.status";
    public static final String EVENT_CHANGE_LANGS_FILTER = "change.languages.status";
    public static final String EVENT_TRANSLATE_RESULT_FILTER = "translate.result";
    public static final String EVENT_AUDIO_DATA_FILTER = "audio.data.message";
    public static final String SAMPLE_RATE_KEY = "sample.rate";
    public static final String NATIVE_TRANSCRIPT_KEY = "native_transcript";
    public static final String SYSTEM_MSG_CODE = "system.message.code";
    public static final String SYSTEM_MSG_DESCRIPTION = "system.message.description";
    public static final String MSG_KEY = "lexifone.broadcast.message";

    public static final String RESPONSE_INVALID = "response.invalid";
    public static final String CHANGE_SETTING_SUCCESS = "CHANGE_SUCCESS";
    public static final String AUDIO_INTERPETER_NOMATCH = "NOMATCH";
    public static final String USER_VALIDATED = "VALIDATED";
    public static final String TEXT_MESSAGE_STATUS = "status";
    public static final String TEXT_MESSAGE_CODE = "code";
    public static final String UNSUPPORTED_STATUS = "unsupported message status: %s";
    public static final String UNSUPPORTED_MESSAGE_TYPE = "unsupported message type: %s";
    public static final String UNSUPPORTED_MESSAGE = "unsupported message: %s";
    public static final String CONNECTION_FAILURE = "failed to create connection";

    public static final String CODE_GENERAL_ERROR = "100";

//    public static final String LEXIFONE_CHINA_SERVER_ADDRESS = "https://china.lexifone.biz:443/lexifone-rt/dynamic/audioHandler";
//    public static final String LEXIFONE_OFFICAL_SERVER_ADDRESS = "https://api.lexifone.biz:443/lexifone-rt/dynamic/audioHandler";
//
//    public static final int CONNECTED = 0x00;
//    public static final int DISCONNECTED = 0x01;
//    public static final int AUTHENTICATE_SUCCESS = 0x02;
//    public static final int TEXT_TRANSLATION_RESULT = 0x03;
//    public static final int AUDIO_TO_TEXT = 0x04;
//    public static final int AUDIO_TO_AUDIO = 0x05;
//    public static final int SWAP_LANGUAGE = 0x06;

    public static final String TEXT_RESULT_KEY = "result";

    public enum connectionStatus {
        NOT_CONNECTED, CONNECTING, CONNECTED, DISCONNECTING;

        public boolean isConnected(){
            return this.ordinal() == CONNECTED.ordinal();
        }

        public boolean notConnected(){
            return this.ordinal() == NOT_CONNECTED.ordinal();
        }
    }

    enum clinetToServerMessageTypes {
        AUTHENTICATION_AND_AUTHORIZATION, CHANGE_LANGUAGES, STOP, TEXT_TRANSLATION, TEXT_TO_SPEECH
    }

    enum serverToClientMessageTypes {
        authorizationStatus, changeLanguagesStatus, binaryTTSFile, textTranslationResults, textToSpeechResults;
    }

    static {
        System.loadLibrary("speex"); // Notice lack of lib prefix
    }

    private final static String LANG_EMPTY_ERROR = "Languages cannot be empty";
    private final static String SAME_LANG_ERROR_PATTERN = "Dialects are of same language: %s";
    private final static String CHANGE_LANGUAGES_SUCCESS = "Successfully changed languages";

    private static LexifoneManager instance = null;
    private static final String TAG = "LexifoneManager";

    private long startConnectionTime = 0;
//    private long translationTime = 0;
    private boolean isMatch;
    private WebSocketConnection webSocketConnection;
    private WebSocketConnectionObserver webSocketConnectionObserver;
    private int retryConnectionCount = 0;
    private boolean gotValidMessage = false;
    private boolean stoppedCalled = false;
    private LexifoneSetting descriptor = new LexifoneSetting();
    LexifoneSettingView lexifoneView;
    connectionStatus connectStatus = connectionStatus.NOT_CONNECTED;
    private Broadcast broadCast;
    private short[] previousData = null;
    private String changeLangNative;
    private String[] changeLangForeign;

    private LexifoneManager() {
    }

    /**
     * @return LexifoneManager singleton instance
     */
    public static LexifoneManager getInstance() {
        if (instance == null) synchronized (LexifoneManager.class) {
            if (instance == null) {
                instance = new LexifoneManager();
            }
        }
        return instance;
    }

    /**
     * @return a map including all supported language codes and its display names
     */
    public Map<String, String> getLanguaeMap(){
        return Language.getDialectMap();
    }

    /**
     * @return connectionStatus representing Lexifone Manager connection status
     */
    public connectionStatus getConnectStatus(){
        return this.connectStatus;
    }

    /**
     * @return the current setting native language code
     */
    public String getNativeLanguage(){
        return descriptor == null ? "" : descriptor.getNativeLanguage();
    }

    /**
     * @return the current setting foreign languages codes array
     */
    public String[] getForeignLanguages(){
        return descriptor == null ? new String[0] : descriptor.getForeignLanguages();
    }

    /**
     * @return integer representing the current setting sample rate
     */
    public int getSampleRate(){
        return descriptor == null ? 0 : descriptor.getSampleRate();
    }

    /**
     * @return true if current setting is to encode audio data
     */
    public boolean isEncoded(){
        return descriptor == null ? false : descriptor.isEncoded();
    }

    /**
     * @return LexifoneSettingView, a read only object that contain the current setting.
     */
    public LexifoneSettingView getSetting(){
        if(lexifoneView == null){
            lexifoneView = new LexifoneSettingView(this.descriptor);
        }
        return lexifoneView;
    }

    /**
     * validate the native and foreign dialect. It is not allowed to use dialects of the same
     * language (e.g. en-us and en-uk)
     * @param nativeDialect - String, the native dialect code
     * @param foreignDialect - String, the foreign dialect code
     * @return null if validation passed, other wise the validation error message
     */
    public String validateLanguages(String nativeDialect, String foreignDialect){
        String nativeLang = Language.langFromDialect(nativeDialect);
        String foreignLang = Language.langFromDialect(foreignDialect);
        if(nativeLang == null || foreignLang == null){
            return LANG_EMPTY_ERROR ;
        }
        if(nativeLang.equals(foreignLang)){
            return String.format(SAME_LANG_ERROR_PATTERN, nativeLang);
        }
        return null;
    }

    /**
     * start Lexifone manager to be ready to process application requests.
     * this will create a connection to Lexifone server and authenticate user details.
     * in case authentication failed the connection is closed immediately.
     * if connection created and authentication passed the manager is now ready
     * for application requests. The connection status is passed asynchronously to
     * the application by broadcast using LexifoneManager.EVENT_CONNECT_FILTER.
     * @param context - Context, the application context
     * @param lexifoneSetting - LexifoneSetting, the session setting as defined by the
     *                        application including user credentials
     * @throws Exception
     */
    public void start(Context context, LexifoneSetting lexifoneSetting) throws Exception {
        Fabric.with(context, new Crashlytics());
        if (connectStatus.isConnected()) {
            Log.w(TAG, "start - already connected");
            return;
        }
        if (!validateSetting()) {
            throw new Exception("Invalid setting");
        }
        lexifoneSetting.setMessageType(clinetToServerMessageTypes.AUTHENTICATION_AND_AUTHORIZATION.name());

        this.descriptor = lexifoneSetting;
        broadCast = new Broadcast(context);
        new Thread(new Runnable() {
            public void run() {
                connectToWs();
            }
        }).start();
    }

    /**
     * stop Lexifone manager from further processing. This will cause disconnection from the server.
     * @throws Exception
     */
    public void stop() throws Exception {
        if (connectStatus.notConnected()) {
            Log.w(TAG, "stop - not connected");
            return;
        }
        sendStopMessage();
    }

    /**
     * translate the given text based on current setting - native and
     * foreign languages. The translation result are passed asynchronously to
     * the application by broadcast using LexifoneManager.EVENT_TRANSLATE_RESULT_FILTER.
     * @param text - String, the text to be translated
     * @throws Exception
     */
    public void translateText(String text) throws Exception {
        Log.d("++++++++++++++", "------->translateText");
        if (isSessionReady() && gotValidMessage) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("messageType", clinetToServerMessageTypes.TEXT_TRANSLATION.name());
            map.put("textToTranslate", text);
            map.put("nativeLanguage", descriptor.getNativeLanguage());
            map.put("foreignLanguages", descriptor.getForeignLanguages());
            ObjectMapper objectMapper = new ObjectMapper();
            sendTextMessage(objectMapper.writeValueAsString(map));
        }
    }

    /**
     * change native and foreign dialects for the current setting. The dialects
     * must pass validation. The status of this request is passed asynchronously to
     * the application by broadcast using LexifoneManager.EVENT_CHANGE_LANGS_FILTER.
     * @param nativeDialect - String, the native dialect code
     * @param foreignDialects - String, the foreign dialect code
     * @throws Exception
     */
    public void changeLanguages(String nativeDialect, String[] foreignDialects) throws Exception {
        if (!validateLanguages(nativeDialect, foreignDialects)) {
            throw new Exception("Invalid dialects - ...");
        }
        if (isSessionReady() && gotValidMessage) {
            this.changeLangNative = nativeDialect;
            this.changeLangForeign = foreignDialects;
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("messageType", clinetToServerMessageTypes.CHANGE_LANGUAGES.name());
            map.put("nativeLanguage", nativeDialect);
            map.put("foreignLanguages", foreignDialects);
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                sendTextMessage(objectMapper.writeValueAsString(map));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * perform text to speech processing. The audio result is passed asynchronously to
     * the application by broadcast using LexifoneManager.EVENT_AUDIO_DATA_FILTER.
     * @param language - String, the language code of the text.
     * @param text - String, the text to speak
     */
    public void textToSpeech(String language, String text) {
        Log.e("++++++++++++++", "------->doTextToSpeechMessage");
        if (isSessionReady() && gotValidMessage) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("messageType", clinetToServerMessageTypes.TEXT_TO_SPEECH.name());
            map.put("ttsText", text);
            map.put("nativeLanguage", language);
            map.put("encoded", descriptor.isEncoded());

            ObjectMapper objectMapper = new ObjectMapper();
            try {
                sendTextMessage(objectMapper.writeValueAsString(map));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * send audio for recognition. The audio data is sent to the server as is (raw data).
     * the translation result are passed asynchronously to
     * the application by broadcast using LexifoneManager.EVENT_TRANSLATE_RESULT_FILTER.
     * if current setting define textOnly as false - and audio result is also passed to
     * the application on the same way as text to speech.
     * @param audioData - byte array, the audio data.
     * @throws Exception
     */
    public void sendAudio(byte[] audioData) throws Exception {
        if (isSessionReady()) {
            webSocketConnection.sendBinaryMessage(audioData);
        }
    }

    /**
     * send audio for recognition. The audio data is first encoded and then sent to
     * the server. the translation result are passed asynchronously to
     * the application by broadcast using LexifoneManager.EVENT_TRANSLATE_RESULT_FILTER.
     * if current setting define textOnly as false - an audio result is also passed to
     * the application on the same way as text to speech result.
     * @param audioData - short array, the audio data.
     * @throws Exception
     */
    public void sendAudioEncoded(short[] audioData, int offset, int length) throws Exception {
        SpeexEncoder encoder = new SpeexEncoder(descriptor.getSampleRate() == 8000 ? FrequencyBand.NARROW_BAND : FrequencyBand.WIDE_BAND, 10);
        short[] effectiveAudioData = null;
        int shortRead = 0;
        if (previousData != null) {
            shortRead = previousData.length + length;
            effectiveAudioData = new short[shortRead];
            System.arraycopy(previousData, 0, effectiveAudioData, 0, previousData.length);
            System.arraycopy(audioData, 0, effectiveAudioData, previousData.length, length);
            previousData = null;
        } else {
            effectiveAudioData = audioData;
            shortRead = length;
        }
        int times = shortRead / encoder.getFrameSize();

        short[] data = new short[encoder.getFrameSize()];
        for (int i = 0; i < times; i++) {
            System.arraycopy(effectiveAudioData, i * data.length, data, 0, data.length);
            byte[] encodedData = encoder.encode(data);
            if (isSessionReady()) {
                webSocketConnection.sendBinaryMessage(encodedData);
            }
        }
        int remainingByte = shortRead % encoder.getFrameSize();
        if (remainingByte > 0) {
            previousData = new short[remainingByte];
            System.arraycopy(effectiveAudioData, times * encoder.getFrameSize(), previousData, 0, remainingByte);
        }
    }


    ///////////////////////////////////////////////////////////////////////////////////////////
    private boolean validateSetting() {
        //TODO - validation
        return true;
    }

    private boolean validateLanguages(String nativeLanguage, String[] foreignLanguages) {
        //TODO - validation
        return true;
    }

    private void connectToWs() {
        Log.e("+++++++++++++", "------>connectToWs");
        stoppedCalled = false;
        gotValidMessage = false;
        try {
            connectStatus = connectionStatus.CONNECTING;
            broadCast.sendConnectStatus(connectStatus.name());
            retryConnectionCount++;
            if (webSocketConnection == null || retryConnectionCount > 3) {
                createConnection();
            } else {
                startConnectionTime = System.currentTimeMillis();
                Log.e("++++++++++++++", "connect begain time :" + startConnectionTime);
                webSocketConnection.reconnect();
            }

        } catch (Exception e) {
            Log.e(TAG, "connectToWs - exception: " + e.getLocalizedMessage());
            stoppedCalled = true;
            broadCast.sendSystemMessage("Error while connecting: " + e.getLocalizedMessage());
            connectStatus = connectionStatus.NOT_CONNECTED;
            broadCast.sendConnectStatus(connectStatus.name());
            broadCast.sendSystemMessage(CONNECTION_FAILURE, CODE_GENERAL_ERROR, e.getLocalizedMessage());
        }
    }

    private void createConnection() throws Exception {
        webSocketConnectionObserver = new WebSocketConnectionObserver() {
            @Override
            public void onOpen() {
                retryConnectionCount = 0;
                System.out.println("Connected!!!!");
                Log.e("++++++++++++++", "connect end time :" + (System.currentTimeMillis() - startConnectionTime));
                userAuthenticate();
            }

            @Override
            public void onClose(WebSocketCloseNotification code, String reason) {
                Log.e(TAG, "Disconnected!");
                stoppedCalled = true;
                if (reason != null) {
                    Log.e(TAG, "close connection reason:" + reason);
//                    broadCast.sendSystemMessage("disconnect reason: " + reason);
                }
                connectStatus = connectionStatus.NOT_CONNECTED;
                broadCast.sendConnectStatus(connectStatus.name());
            }

            @Override
            public void onTextMessage(String payload) {
                Log.e(TAG, String.format("Got string message! %s", payload));
                try {
                    ObjectMapper objectMapper = new ObjectMapper();
                    @SuppressWarnings("unchecked")
                    Map<String, String> map = objectMapper.readValue(payload, HashMap.class);
                    String messageType = map.get("messageType");
                    String status = map.get(TEXT_MESSAGE_STATUS);
                    // makes sure that the first message from server includes a verified status.
                    // In case of a verified status, starts to capture audio from the microphone
                    // In case of none verified status stops the session
                    if (messageType == null) {
                        broadCast.sendSystemMessage(RESPONSE_INVALID, CODE_GENERAL_ERROR, "message type empty");
                    } else if (!gotValidMessage) {
                        if (messageType.equals(serverToClientMessageTypes.authorizationStatus.name())) {
                            if (status.equals(USER_VALIDATED)) {
                                gotValidMessage = true;
                                Log.d(TAG, "User is validated");
                                connectStatus = connectionStatus.CONNECTED;
                                broadCast.sendConnectStatus(connectStatus.name());
                                broadCast.sendSystemMessage("Connected and validated successfully");
                            } else if (!status.equals(USER_VALIDATED)) {
                                Log.d(TAG, "User is not validated - aborting");
                                broadCast.sendSystemMessage("Session not valid - disconnecting", map.get(TEXT_MESSAGE_CODE), status);
                                sendStopMessage();
                            } else {
                                broadCast.sendSystemMessage(RESPONSE_INVALID, CODE_GENERAL_ERROR, String.format(UNSUPPORTED_STATUS, status));
                            }
                        }
                    } else {
                        if (messageType.equals(serverToClientMessageTypes.textTranslationResults.name())) {
                            String nativeString = map.get(descriptor.getNativeLanguage());//TODO - add native language in response
                            Log.e(TAG, "nativeString:" + nativeString);
                            isMatch = !AUDIO_INTERPETER_NOMATCH.equals(nativeString);
                            Log.e(TAG, "----->isMatch:" + isMatch);
                            Map<String, String> result = new HashMap<String, String>();
                            for (String key : map.keySet()) {
                                if (!Language.isValidDialect(key) || key.equals(descriptor.getNativeLanguage())) {
                                    continue;
                                }
                                String val = map.get(key);
                                result.put(key, val);
                            }
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(TEXT_RESULT_KEY, (Serializable) result);
                            broadCast.sendTranslateResultMsg(nativeString, bundle);
                        } else if (messageType.equals(serverToClientMessageTypes.changeLanguagesStatus.name())) {
                            if (CHANGE_SETTING_SUCCESS.equals(status)) {
                                descriptor.setNativeLanguage(changeLangNative);
                                descriptor.setForeignLanguages(changeLangForeign);
                                broadCast.sendSystemMessage(CHANGE_LANGUAGES_SUCCESS);
                            }
                            broadCast.sendChangeLanguagesStatus(status);
                        } else if (messageType.equals(serverToClientMessageTypes.textToSpeechResults.name()) ||
                                messageType.equals(serverToClientMessageTypes.binaryTTSFile.name())) {
                            // nothing to do currently
                        }
                        else {
                            broadCast.sendSystemMessage(RESPONSE_INVALID, CODE_GENERAL_ERROR, String.format(UNSUPPORTED_MESSAGE_TYPE, messageType));
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage(), e);
                    broadCast.sendSystemMessage(RESPONSE_INVALID, CODE_GENERAL_ERROR, e.getLocalizedMessage());
                }
            }

            @Override
            public void onRawTextMessage(byte[] payload) {
                broadCast.sendSystemMessage(RESPONSE_INVALID, CODE_GENERAL_ERROR, String.format(UNSUPPORTED_MESSAGE, "RawTextMessage"));
            }

            @Override
            public void onBinaryMessage(byte[] payload) {
                Log.d(TAG, "payload size:" + payload.length);
                if (isMatch && payload.length > 44) {
                    try {
                        Bundle bundle = new Bundle();
                        if (descriptor.isEncoded()) {
                            SpeexDecoder decoder = new SpeexDecoder(FrequencyBand.WIDE_BAND);
                            int decodedLen = 0;
                            ByteArrayOutputStream out = new ByteArrayOutputStream();
                            while (decodedLen < payload.length) {
                                byte[] encodedAudio = new byte[42];
                                System.arraycopy(payload, decodedLen, encodedAudio, 0, Math.min(encodedAudio.length, payload.length - decodedLen));
                                decodedLen += encodedAudio.length;
                                short[] decoded = decoder.decode(encodedAudio);
                                byte[] decodedBytes = ShortToByte_Twiddle_Method(decoded);
                                out.write(decodedBytes);
                            }
                            broadCast.sendAudioData(out.toByteArray());
                        } else {
                            byte[] pcmData = new byte[payload.length - 44];
                            System.arraycopy(payload, 44, pcmData, 0, pcmData.length);
                            broadCast.sendAudioData(pcmData);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "onBinaryMessage - exception: " + e.getLocalizedMessage());
                    }
                }
            }
        };

        this.webSocketConnection = new WebSocketConnection();
        WebSocketOptions options = new WebSocketOptions();
        options.setMaxMessagePayloadSize(1024 * 1024 * 10);
        options.setMaxFramePayloadSize(1024 * 1024 * 5);
        startConnectionTime = System.currentTimeMillis();
        Log.e("++++++++++++++", "connect begain time :" + startConnectionTime);
        Log.e("++++++++++++++", "connect url :" + descriptor.getWsUrl());
        this.webSocketConnection.connect(new URI(descriptor.getWsUrl()), webSocketConnectionObserver, options);
    }

    private synchronized void sendTextMessage(String message) {
        if (isSessionReady()) {
            this.webSocketConnection.sendTextMessage(message);
        }
    }

    private boolean isSessionReady() {
        return this.webSocketConnection != null && this.webSocketConnection.isConnected();
    }

    private byte[] ShortToByte_Twiddle_Method(short[] input) {
        int short_index, byte_index;
        int iterations = input.length;

        byte[] buffer = new byte[input.length * 2];

        short_index = byte_index = 0;

        for (/* NOP */; short_index != iterations; /* NOP */) {
            buffer[byte_index] = (byte) (input[short_index] & 0x00FF);
            buffer[byte_index + 1] = (byte) ((input[short_index] & 0xFF00) >> 8);

            ++short_index;
            byte_index += 2;
        }

        return buffer;
    }

    // Stops the session.
    private void sendStopMessage() throws Exception {
        if (!stoppedCalled) {
            Log.d(TAG, "Calling STOP message");
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("messageType", clinetToServerMessageTypes.STOP.name());
            ObjectMapper objectMapper = new ObjectMapper();
            Log.d(TAG, objectMapper.writeValueAsString(map));
            if (isSessionReady()) {
                connectStatus = connectionStatus.DISCONNECTING;
                broadCast.sendConnectStatus(connectStatus.name());
                this.webSocketConnection.sendTextMessage(objectMapper.writeValueAsString(map));
            }
            stoppedCalled = true;
            if (isSessionReady()) {
                this.webSocketConnection.disconnect();
            }
        }
    }

    private void userAuthenticate() {
        if (isSessionReady() && !gotValidMessage) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("userName", descriptor.getUserName());
            map.put("password", descriptor.getPassword());
            map.put("sampleRate", descriptor.getSampleRate());
            map.put("nativeLanguage", descriptor.getNativeLanguage());
            map.put("foreignLanguages", descriptor.getForeignLanguages());
            map.put("performTTS", descriptor.getPerformTTS());
            map.put("encoded", descriptor.isEncoded());
            if(descriptor.getSpeakerId() != null){
                map.put("speakerId" , descriptor.getSpeakerId());
            }
            map.put("messageType", clinetToServerMessageTypes.AUTHENTICATION_AND_AUTHORIZATION.name());
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                sendTextMessage(objectMapper.writeValueAsString(map));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

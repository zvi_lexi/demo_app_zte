package com.lexifone.sdk;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Zvi on 5/19/2015.
 */
class Language {
    private static HashMap<String, String> dialectMap = new HashMap<>();
    private static HashMap<String, String> languageDialectMap = new HashMap<>();

    static {
        load();
    }

    public static Map<String, String> getDialectMap(){
        return new HashMap<>(dialectMap);
    }

    public static String langFromDialect(String dialect){
        if(dialect == null){
            return "";
        }
        String lang = null;
        int pos = dialect.indexOf("-");
        if(pos > 0 && (lang = languageDialectMap.get(dialect.substring(0, pos))) != null) {
            return lang;
        }
        return dialectMap.get(dialect);
    }

    public static boolean isValidDialect(String dialect){
        return dialectMap.get(dialect) != null;
    }

//    <entry key="en-us" value="eng-USA" /> <!-- English -->
//    <entry key="en-gb" value="eng-GBR" /> <!-- English UK -->
//    <entry key="en-au" value="eng-AUS" /> <!-- English Australian -->
//    <entry key="es-es" value="spa-ESP" /> <!-- Spanish -->
//    <entry key="es-mx" value="spa-XLA" /> <!-- Spanish Americas -->
//    <entry key="de-de" value="deu-DEU" /> <!-- German -->
//    <entry key="it-it" value="ita-ITA" /> <!-- Italian -->
//    <entry key="fr-fr" value="fra-FRA" /> <!-- French -->
//    <entry key="fr-ca" value="fra-CAN" /> <!-- French Canadian -->
//    <entry key="pt-br" value="por-BRA" /> <!-- Portuguese Brazil -->
//    <entry key="pt-pt" value="por-PRT" /> <!-- Portuguese -->
//    <entry key="zh-cn" value="cmn-CHN" /> <!-- Mandarine -->
//    <entry key="ru-ru" value="rus-RUS" /> <!-- Russian -->
//    <entry key="he-il" value="heb-ISR" /> <!-- Hebrew -->
//    <entry key="pl-pl" value="pol-POL" /> <!-- Polish -->
//    <entry key="zh-tw" value="cmn-TWN" /> <!-- Taiwanese Mandarin -->
//    <entry key="en-in" value="eng-IND" /> <!-- Indian English -->

    private static void load(){
        dialectMap.put("en-us", "English");
        dialectMap.put("en-au", "English Australian");
        dialectMap.put("en-gb", "English UK");
        dialectMap.put("es-es", "Sapnish");
        dialectMap.put("es-mx", "Spanish Americas");
        dialectMap.put("de-de", "German");
        dialectMap.put("it-it", "Italian");
        dialectMap.put("fr-fr", "French");
        dialectMap.put("fr-ca", "French Canadian");
        dialectMap.put("pt-pt", "Portuguese");
        dialectMap.put("pt-br", "Portuguese Brazil");
        dialectMap.put("zh-cn", "Mandarine");
        dialectMap.put("ru-ru", "Russian");
        dialectMap.put("he-il", "Hebrew");
        dialectMap.put("pl-pl", "Polish");
        dialectMap.put("zh-tw", "Mandarin Taiwanese");
        dialectMap.put("en-in", "English Indian");

        // load dialect to language values
        languageDialectMap.put("en", "English");
        languageDialectMap.put("es", "Spanish");
        languageDialectMap.put("fr", "French");
        languageDialectMap.put("pt", "Portuguese");
        languageDialectMap.put("zh", "Mandarin");
    }
}

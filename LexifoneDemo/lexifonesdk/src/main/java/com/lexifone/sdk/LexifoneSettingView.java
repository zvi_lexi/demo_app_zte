package com.lexifone.sdk;

/**
 * Created by Zvi on 5/18/2015.
 */
public class LexifoneSettingView {

    private LexifoneSetting setting;

    LexifoneSettingView(){
        this(new LexifoneSetting());
    }

    LexifoneSettingView(LexifoneSetting setting){
        this.setting = setting == null ? new LexifoneSetting() : setting;
    }

    public String getServerAddress() {
        return setting.getServerAddress();
    }

    public String getNativeLanguage() {
        return setting.getNativeLanguage();
    }

    public String[] getForeignLanguages() {
        return setting.getForeignLanguages();
    }

    public String getUserName() {
        return setting.getUserName();
    }

    public String getPassword() {
        return setting.getPassword();
    }

    public boolean getPerformTTS() {
        return setting.getPerformTTS();
    }

    public int getSampleRate() {
        return setting.getSampleRate();
    }

    public boolean isEncoded() {
        return setting.isEncoded();
    }
}

package com.lexifone.sdk;

import java.util.Arrays;

/**
 * the class represents the setting neededby LexifoneManaager
 * to process application requests
 */
public class LexifoneSetting {
    private static final String URL_PATTERN = "wss://%s/lexifone-rt/dynamic/audioHandler";
    private static final String URL_PROXY_PATTERN = "wss://%s/lexifone-proxy/dynamic/proxyAudioHandler";

    private String serverAddress;
    private String wsUrl;
    private String nativeLanguage;
    private String[] foreignLanguages;
    private String userName;
    private String password;
    private boolean performTTS;
    private int sampleRate;
    private String messageType;
    private boolean encoded = true;
    private String speakerId;


    private boolean useProxy = false;

    String getWsUrl() {
        return String.format(useProxy ? URL_PROXY_PATTERN : URL_PATTERN, serverAddress);
    }

    public String getServerAddress() {
        return serverAddress;
    }

    public void setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
    }

    public String getNativeLanguage() {
        return nativeLanguage;
    }

    public void setNativeLanguage(String nativeLanguage) {
        this.nativeLanguage = nativeLanguage;
    }

    public String[] getForeignLanguages() {
        return foreignLanguages;
    }

    public void setForeignLanguages(String[] foreignLanguages) {
        this.foreignLanguages = foreignLanguages;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean getPerformTTS() {
        return performTTS;
    }

    public void setPerformTTS(boolean flag) {
        this.performTTS = flag;
    }

    public int getSampleRate() {
        return sampleRate;
    }

    public void setSampleRate(int sampleRate) {
        this.sampleRate = sampleRate;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String type) {
        messageType = type;
    }

    public boolean isEncoded() {
        return encoded;
    }

    public void setEncoded(boolean encoded) {
        this.encoded = encoded;
    }

    public boolean isUseProxy() {
        return useProxy;
    }

    public void setUseProxy(boolean useProxy) {
        this.useProxy = useProxy;
    }

    public String getSpeakerId() {
        return speakerId;
    }

    public void setSpeakerId(String speakerId) {
        this.speakerId = speakerId;
    }

    @Override
    public String toString() {
        return "LexifoneSetting{" +
                "serverAddress='" + serverAddress + '\'' +
                ", wsUrl='" + wsUrl + '\'' +
                ", nativeLanguage='" + nativeLanguage + '\'' +
                ", foreignLanguages=" + Arrays.toString(foreignLanguages) +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", performTTS=" + performTTS +
                ", sampleRate=" + sampleRate +
                ", messageType='" + messageType + '\'' +
                ", encoded=" + encoded +
                ", speakerId='" + speakerId + '\'' +
                ", useProxy=" + useProxy +
                '}';
    }
}

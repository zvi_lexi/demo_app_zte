package com.lexifone.app.lexifonedemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.telephony.TelephonyManager;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.lexifone.sdk.LexifoneManager;
import com.lexifone.sdk.LexifoneSetting;

import java.io.ByteArrayInputStream;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static com.lexifone.app.lexifonedemo.R.color.green;


public class MainActivity extends ActionBarActivity implements View.OnTouchListener {
    private static String TAG = "MainActivity";
    public static final String NEW_LINE = System.getProperty("line.separator");
    private static boolean FIRST_CREATE = true;
    private static boolean REGISTERED = false;

    private static LexifoneManager lexifoneManager = LexifoneManager.getInstance();
    private Map<String, String> languageMap;

    public static final int AUDIO_SOURCE = MediaRecorder.AudioSource.MIC;
    public static final int CHANNEL_IN_CONFIG = AudioFormat.CHANNEL_IN_MONO;
    public static final int AUDIO_FORMAT = AudioFormat.ENCODING_PCM_16BIT;

    final int DEFAULT_SAMPLE_RATE = 8000;
    final int TEST_CONF = AudioFormat.CHANNEL_OUT_MONO;
    final int TEST_FORMAT = AudioFormat.ENCODING_PCM_16BIT;
    final int TEST_MODE = AudioTrack.MODE_STREAM;
    final int TEST_STREAM_TYPE = AudioManager.STREAM_MUSIC;

    String nativeLanguage, foreignLanguage;
    boolean settingChanged;
    private boolean captureAudio;

    TextView viewConnect, viewTranslation, viewNativeLang, viewNativeText;
    Button btStartStop, btTalk, btTranslate, btSwapLangs;
    ProgressBar progressBar = null;
    int disconnectColor;

    SharedPreferences sharedPreferences;
    private BroadcastReceiver mSystemMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            handleSystemMessage(intent);
        }
    };

    private BroadcastReceiver mTranslationMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            handleTranslateResult(intent);
        }
    };

    private BroadcastReceiver mAudioMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            byte[] data = intent.getByteArrayExtra(lexifoneManager.MSG_KEY);
            int sampleRate = intent.getIntExtra(lexifoneManager.SAMPLE_RATE_KEY, DEFAULT_SAMPLE_RATE);
            playMedia(data, sampleRate);
        }
    };

    private BroadcastReceiver mStatusMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d("receiver", "Got action: " + action);
            if (lexifoneManager.EVENT_CONNECT_FILTER.equals(action)) {
                handleConnectStatusMsg(intent);
            } else if (lexifoneManager.EVENT_CHANGE_LANGS_FILTER.equals(action)) {
                handleChangeLanguagesResult(intent);
            } else {
                Log.e("receiver", "unsupported action: " + action);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        PreferenceManager.setDefaultValues(this, R.xml.pref_general, true);
        synchronized (this) {
            if (!REGISTERED) {
                REGISTERED = true;
                registerListeners();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(lexifoneManager.getConnectStatus().isConnected()){
            updateLanguages();
        }else {
            init();
        }
        if(FIRST_CREATE){
            FIRST_CREATE = false;
            start_stop(null);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
            settingChanged = false;
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        synchronized (this) {
            unregisterListeners();
            REGISTERED = false;
        }
        if (lexifoneManager != null && lexifoneManager.getConnectStatus().isConnected()) {
            try {
                lexifoneManager.stop();
            } catch (Exception e) {
                Log.e(TAG, "failed to stop lexifoneManager");
            }
        }
        super.onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (v.getId() == R.id.talk_bt) {
                    if(captureAudio){ return true;}
                    v.setBackgroundResource(R.drawable.audio_button_down);
                    try {
                        startCaptureAudio();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
                if (v.getId() == R.id.talk_bt) {
                    v.setBackgroundResource(R.drawable.audio_button_normal);
                    stopCaptureAudio();
                }
                break;
        }
        return true;
    }

    public void start_stop(View v) {
        progressBar.setVisibility(View.VISIBLE);
        try {
            LexifoneManager.connectionStatus connStatus = lexifoneManager.getConnectStatus();
            if (connStatus.notConnected()) {
                lexifoneManager.start(getApplicationContext(), loadLexifoneSetting());
            } else if (connStatus.isConnected()){
                lexifoneManager.stop();
            }else{
                Log.i(TAG, "start_stop - do nothing, Connection status is: " + connStatus.name());
            }
        } catch (Exception e) {
            e.printStackTrace();
            viewConnect.setText(e.getLocalizedMessage());
            progressBar.setVisibility(View.GONE);
        }
    }

    public void translateText(View view) {
        String textToTranslate = viewNativeText.getText().toString();
        if(textToTranslate.length() > 0){
            try {
                lexifoneManager.translateText(textToTranslate);
                InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                displayToast(getString(R.string.performing_translation));
            } catch (Exception e) {
                handleError(e);
            }
        }
    }

    public void swap_languages(View v) {
        updateLanguages(foreignLanguage, nativeLanguage);
    }

    public void clear_translations(View v) {
        viewTranslation.setText("");
        resultBuilder.setLength(0);
    }

    ///////////////////
    /// private methods
    ///////////////////

    private void handleChangeLanguagesResult(Intent intent) {
        if (LexifoneManager.CHANGE_SETTING_SUCCESS.equals(intent.getStringExtra(lexifoneManager.MSG_KEY))) {
            setLanguages();
        } else {
            displayToast(R.string.change_languages_failed + intent.getStringExtra(lexifoneManager.MSG_KEY));
        }
        toggleActionButtons(true);
        progressBar.setVisibility(View.GONE);
    }

    private void updateLanguages() {
        String prefNativeLanguage = sharedPreferences.getString(SettingsActivity.PREF_NATIVE_LANG_NAME, getString(R.string.nativeLanguage));
        String prefForeignLanguage = sharedPreferences.getString(SettingsActivity.PREF_FOREIGN_LANG_NAME, getString(R.string.foreignLanguage));

        if (!prefNativeLanguage.equals(nativeLanguage) || !prefForeignLanguage.equals(foreignLanguage)) {
            updateLanguages(prefNativeLanguage, prefForeignLanguage);
        }
    }

    private void updateLanguages(String updateNativeLang, String updatedForeignLang){
        toggleActionButtons(false);
        progressBar.setVisibility(View.VISIBLE);
        try {
            lexifoneManager.changeLanguages(updateNativeLang, new String[]{updatedForeignLang});
        } catch (Exception e) {
            handleError(e);
            toggleActionButtons(true);
            progressBar.setVisibility(View.GONE);
        }
    }

    private void setLanguages(){
        nativeLanguage = lexifoneManager.getNativeLanguage();
        String[] foreignLangs = lexifoneManager.getForeignLanguages();
        foreignLanguage = foreignLangs.length > 0 ? foreignLangs[0] : "";
        viewNativeLang.setText(languageMap.get(nativeLanguage));
        btTranslate.setText(getString(R.string.lexifone_translate) + languageMap.get(foreignLanguage));
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(SettingsActivity.PREF_NATIVE_LANG_NAME, nativeLanguage);
        editor.putString(SettingsActivity.PREF_FOREIGN_LANG_NAME, foreignLanguage);
        editor.commit();
    }

    private void handleConnectStatusMsg(Intent intent) {
        try {
            LexifoneManager.connectionStatus status = LexifoneManager.connectionStatus.valueOf(intent.getStringExtra(lexifoneManager.MSG_KEY));
            btTalk.setEnabled(false);
            switch (status) {
                case CONNECTED:
                    viewConnect.setText(R.string.user_validate);
                    viewConnect.setTextColor(green);
                    btStartStop.setText(getString(R.string.lexifone_stop));
                    btStartStop.setEnabled(true);
                    toggleActionButtons(true);
                    progressBar.setVisibility(View.GONE);
                    break;
                case NOT_CONNECTED:
                    viewConnect.setText(R.string.disconnected);
                    viewConnect.setTextColor(disconnectColor);
                    btStartStop.setText(getString(R.string.lexifone_start));
                    btStartStop.setEnabled(true);
                    toggleActionButtons(false);
                    progressBar.setVisibility(View.GONE);
                    break;
                case CONNECTING:
                    viewConnect.setText(R.string.lexifone_connecting);
                    btStartStop.setText(getString(R.string.lexifone_connecting));
                    break;
                case DISCONNECTING:
                    viewConnect.setText(R.string.lexifone_disconnecting);
                    btStartStop.setText(getString(R.string.lexifone_disconnecting));
                    btStartStop.setEnabled(false);
                    toggleActionButtons(false);
                    break;
            }
        } catch (Exception e) {
            viewConnect.setText(intent.getStringExtra(lexifoneManager.MSG_KEY));
            displayToast(R.string.invalid_connect_status + intent.getStringExtra(lexifoneManager.MSG_KEY), Toast.LENGTH_LONG);
        }
    }

    StringBuilder resultBuilder = new StringBuilder();
    private void handleTranslateResult(Intent intent) {
        String transcript = intent.getStringExtra(lexifoneManager.NATIVE_TRANSCRIPT_KEY);
        if(LexifoneManager.AUDIO_INTERPETER_NOMATCH.equals(transcript)){
            displayToast(R.string.voice_recognition_failed, Toast.LENGTH_LONG);
        }else {
            Bundle bundle = intent.getBundleExtra(lexifoneManager.MSG_KEY);
            Map<String, String> map = (Map<String, String>) bundle.getSerializable(LexifoneManager.TEXT_RESULT_KEY);
            String translated = map.get(foreignLanguage);
            if(!lexifoneManager.getSetting().getPerformTTS()){
                // do text to speech to play the translated text
                lexifoneManager.textToSpeech(foreignLanguage, translated);
            }
            resultBuilder.append(transcript + NEW_LINE);
            resultBuilder.append(translated + NEW_LINE);
            viewTranslation.setText(resultBuilder);
            viewNativeText.setText("");
            final int scrollAmount = viewTranslation.getLayout().getLineTop(viewTranslation.getLineCount()) - viewTranslation.getHeight();
            // if there is no need to scroll, scrollAmount will be <=0
            if (scrollAmount > 0)
                viewTranslation.scrollTo(0, scrollAmount);
            else
                viewTranslation.scrollTo(0, 0);
        }
    }

    private void handleSystemMessage(Intent intent) {
        String message = intent.getStringExtra(lexifoneManager.MSG_KEY);
        String description = intent.getStringExtra(lexifoneManager.SYSTEM_MSG_DESCRIPTION);
        if (description != null && description.length() > 0){
            message += NEW_LINE + description;
        }
        displayToast(message, Toast.LENGTH_LONG);
        if(LexifoneManager.RESPONSE_INVALID.equals(message)){
            progressBar.setVisibility(View.GONE);
        }
    }

    private void startCaptureAudio() {
        Log.e(TAG, "--------------->startCaptureAudio");
        this.captureAudio = true;
        new Thread(new Runnable() {
            public void run() {
                int sampleRate = lexifoneManager.getSampleRate();
                int buffersize = AudioRecord.getMinBufferSize(sampleRate, CHANNEL_IN_CONFIG, AUDIO_FORMAT);
                AudioRecord recorder = new AudioRecord(AUDIO_SOURCE, sampleRate, CHANNEL_IN_CONFIG, AUDIO_FORMAT, buffersize);
                recorder.startRecording();
                if (lexifoneManager.isEncoded()) {
                    short audioData[] = new short[buffersize];
                    while (captureAudio) {
                        int status = recorder.read(audioData, 0, audioData.length);
                        if (status == AudioRecord.ERROR_INVALID_OPERATION || status == AudioRecord.ERROR_BAD_VALUE) {
                            continue;
                        }
                        try {
                            lexifoneManager.sendAudioEncoded(audioData, 0, status);
                        } catch (Exception e) {
                            Log.e(TAG, "exception sending data: " + e.getLocalizedMessage());
                        }
                        audioData = new short[buffersize];
                    }
                } else {
                    byte audioData[] = new byte[buffersize];
                    while (captureAudio) {
                        int status = recorder.read(audioData, 0, audioData.length);
                        if (status == AudioRecord.ERROR_INVALID_OPERATION || status == AudioRecord.ERROR_BAD_VALUE) {
                            continue;
                        }
                        try {
                            lexifoneManager.sendAudio(audioData);
                        } catch (Exception e) {
                            Log.e(TAG, "exception sending data: " + e.getLocalizedMessage());
                        }
                        audioData = new byte[buffersize];
                    }
                }
                recorder.stop();
                recorder.release();
            }
        }).start();
    }

    private void stopCaptureAudio() {
        Log.e(TAG, "--------------->stopCaptureAudio");
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    captureAudio = false;
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void playMedia(byte[] data, int sampleRate) {
        try {
            int minBuffSize = AudioTrack.getMinBufferSize(sampleRate, TEST_CONF, TEST_FORMAT);
            AudioTrack track = new AudioTrack(TEST_STREAM_TYPE, sampleRate, TEST_CONF, TEST_FORMAT, minBuffSize, TEST_MODE);
            byte[] temp = new byte[minBuffSize];
            ByteArrayInputStream ios = new ByteArrayInputStream(data);
            int byteRead = ios.read(temp);
            track.play();
            while (byteRead > 0) {
                track.write(temp, 0, byteRead);
                byteRead = ios.read(temp);
            }

            track.release();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void displayToast(String message) {
        displayToast(message, Toast.LENGTH_SHORT);
    }

    private void displayToast(int resId) {
        displayToast(getText(resId).toString(), Toast.LENGTH_SHORT);
    }

    private void displayToast(int resId, int duration) {
        displayToast(getText(resId).toString(), duration);
    }

    private void displayToast(String message, int duration) {
        Context context = getApplicationContext();
        if (context == null || message == null || message.length() == 0) {
            return;
        }
        Toast toast = Toast.makeText(context, message, duration);
        toast.show();
    }

    private void registerListeners() {
        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
        IntentFilter filter = new IntentFilter(lexifoneManager.EVENT_CONNECT_FILTER);
        filter.addAction(lexifoneManager.EVENT_CHANGE_LANGS_FILTER);
        broadcastManager.registerReceiver(mStatusMessageReceiver, filter);

        broadcastManager.registerReceiver(mSystemMessageReceiver, new IntentFilter(lexifoneManager.EVENT_SYSTEM_MSG_FILTER));
        broadcastManager.registerReceiver(mAudioMessageReceiver, new IntentFilter(lexifoneManager.EVENT_AUDIO_DATA_FILTER));
        broadcastManager.registerReceiver(mTranslationMessageReceiver, new IntentFilter(lexifoneManager.EVENT_TRANSLATE_RESULT_FILTER));
    }

    private void unregisterListeners() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
        broadcastManager.unregisterReceiver(mStatusMessageReceiver);
        broadcastManager.unregisterReceiver(mTranslationMessageReceiver);
        broadcastManager.unregisterReceiver(mAudioMessageReceiver);
    }

    private void init() {
        languageMap = LexifoneManager.getInstance().getLanguaeMap();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        nativeLanguage = sharedPreferences.getString(SettingsActivity.PREF_NATIVE_LANG_NAME, getString(R.string.nativeLanguage));
        foreignLanguage = sharedPreferences.getString(SettingsActivity.PREF_FOREIGN_LANG_NAME, getString(R.string.foreignLanguage));

        // UI init
        viewConnect = (TextView) findViewById(R.id.connect_status_tv);
        viewConnect.setText(getString(R.string.disconnected));
        disconnectColor = viewConnect.getCurrentTextColor();
        viewTranslation = (TextView) findViewById(R.id.translation_tv);
        viewTranslation.setMovementMethod(new ScrollingMovementMethod());
        viewNativeLang = (TextView) findViewById(R.id.native_lang_title_tv);
        viewNativeLang.setText(languageMap.get(nativeLanguage));
        viewNativeText = (TextView) findViewById(R.id.native_text_et);

        btTalk = (Button) findViewById(R.id.talk_bt);
        btTalk.setOnTouchListener(this);
        btSwapLangs = (Button) findViewById(R.id.swap_languages_bt);
        btTranslate = (Button) findViewById(R.id.translate_bt);
        btTranslate.setText(getString(R.string.lexifone_translate) + languageMap.get(foreignLanguage));
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        btStartStop = (Button) findViewById(R.id.start_stop_bt);
        btStartStop.setFocusable(true);
        btStartStop.setFocusableInTouchMode(true);///add this line
        btStartStop.requestFocus();

        toggleActionButtons(false);
    }

    private void toggleActionButtons(boolean enable){
        btTalk.setEnabled(enable);
        btSwapLangs.setEnabled(enable);
        btTranslate.setEnabled(enable);
    }

    private LexifoneSetting loadLexifoneSetting(){
        // load session values from res/values/lexifone_setting.xml file
        LexifoneSetting setting = new LexifoneSetting();

        setting.setServerAddress(getString(R.string.serverAddress));
        setting.setUserName(getString(R.string.userName));
        setting.setPassword(getString(R.string.password));
        setting.setNativeLanguage(nativeLanguage);
        setting.setForeignLanguages(new String[]{foreignLanguage});
        Resources res = getResources();
        setting.setSampleRate(res.getInteger(R.integer.sampleRate));
        setting.setEncoded(res.getBoolean(R.bool.encoded));
        setting.setPerformTTS(res.getBoolean(R.bool.textOnly) == false);
        setting.setUseProxy(res.getBoolean(R.bool.useProxy));
        setting.setSpeakerId(getDeviceUniqueId());

        return setting;
    }
    private String getDeviceUniqueId(){
        String identifier = null;
        TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        if (tm != null) {
            identifier = tm.getDeviceId();
        }
        if (identifier == null || identifier .length() == 0) {
            identifier = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        return identifier;
    }

    private void handleError(Exception e) {
        e.printStackTrace();
        handleError(e.getMessage());
    }

    private void handleError(String message){
        Log.e(TAG, "Error in application: " + message);
        displayToast(message);
    }
}
